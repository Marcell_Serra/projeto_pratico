<!DOCTYPE html>
<html lang="pt-br">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
     <title>GERENCIADOR DE TAREFAS</title>
    <link rel="stylesheet" href="style/css/style.css" />
<!-- 	 <link rel="stylesheet" href="style/css/bootstrap.css"> -->
</head>
<body >
	<?php 
	// USE SEUS MÉTODOS ORIUNDOS DA CLASSE ControllerLogin.php
	
    ?>
    
    <div class="login-page">
 		 <div class="form">
   			<form action="login.php" method="get" class="login-form">
     		 <input type="text" name="login" placeholder="login"/>
      		 <input type="password" name="senha" placeholder="senha"/>
      		 <button type="submit">Acesse</button>
      			<p class="message">Já possue cadastro? <a href="./cadastro.php">Faça seu Cadastro aqui</a></p>
   		    </form>
 		 </div>
	</div>
    <script src="style/js/jquery-3.3.1.slim.min.js"></script>
    <script src="style/js/formulario.js"></script>
</body>
</html>